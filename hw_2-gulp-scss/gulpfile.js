const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const clean = require('gulp-clean');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const imgMin = require('gulp-imagemin');
const cleanCss = require('gulp-clean-css');
const uglify = require('gulp-uglify');


const path = {
    src: {
        scss: "./src/scss/**/*.scss",
        js: "./src/js/*.js",
        img: "./src/img/**/*",
        html: "./index.html"
    },
    dist: {
        css: "./dist/css",
        js: "./dist/js",
        img: "./dist/img",
        root: "./dist"
    }
};

// fun

const cleanDist = function () {
    return gulp.src(path.dist.root, {allowEmpty: true})
        .pipe(clean());
};

const createStyle = () => {
    return gulp.src(path.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCss({compatibility: 'ie8'}))
        .pipe(autoprefixer({browsers: 'last 3 versions'}))
        .pipe(gulp.dest(path.dist.css))
        .pipe(browserSync.stream())
};

const createImg = () => {
    return gulp.src(path.src.img)
        .pipe(imgMin())
        .pipe(gulp.dest(path.dist.img))

};
const creatJs = () => {
    return gulp.src(path.src.js)
        .pipe(concat('script.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(path.dist.js))
        .pipe(browserSync.stream())
};


// watch

const watcher = function () {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch(path.src.scss, createStyle).on('change', browserSync.reload);
    gulp.watch(path.src.img, createImg).on('change', browserSync.reload);
    gulp.watch(path.src.html).on('change', browserSync.reload);
};

// task

gulp.task('build', gulp.series(cleanDist, createStyle, createImg, creatJs));
gulp.task('dev', watcher);
